//
//  AuthManager.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import Foundation
import SwiftyKeychainKit

class AuthManager {
    
    static let shared = AuthManager()
    private init() {}
    
    private var keychain = Keychain(service: "com.shakh.Gallery")
    private let key = KeychainKey<String>(key: "keychainKey")
    private let userInfoKey = "userInfo"
    
    let maxAttemptsCount = 3
    var attemptsCount = 0
    
    var timer: Timer?
    let timeInterval = 30
   
    func registerUser(userName: String, login: String, password: String) {
        let userInfo = UserInfo(userName: userName, login: login)
        UserDefaults.standard.set(encodable: userInfo, forKey: userInfoKey)
        try? keychain.set(password, for: key)
    }
    
    func login(login: String, password: String) -> Bool {
        let userInfo = UserDefaults.standard.value(UserInfo.self, forKey: userInfoKey)
        let savedPassword = try? keychain.get(key)
        if userInfo?.login == login,
           savedPassword == password {
            attemptsCount = 0
            return true
        }
        attemptsCount += 1
        if attemptsCount == maxAttemptsCount {
            lockAccount()
        }
        return false
    }
    
    func getUserName() -> String?  {
        let userInfo = UserDefaults.standard.value(UserInfo.self, forKey: userInfoKey)
        return userInfo?.userName
    }
    
    func getUserLogin() -> String? {
        let userInfo = UserDefaults.standard.value(UserInfo.self, forKey: userInfoKey)
        return userInfo?.login
    }
    
    func lockAccount() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: Double(timeInterval),
                                          target: self,
                                          selector: #selector(onTimerFires),
                                          userInfo: nil,
                                          repeats: false)
        sendLockAccountNotification()
    }
    
    @objc func onTimerFires() {
        attemptsCount = 0
        NotificationCenter.default.post(name: .accountUnLocked, object: nil)
    }
    
    func sendLockAccountNotification() {
        NotificationCenter.default.post(name: .accountLocked, object: nil)
    }
}

// MARK: - Notication.Name

extension NSNotification.Name {
    static let accountLocked = NSNotification.Name("AccountLockedNotification")
    static let accountUnLocked = NSNotification.Name("AccountUnlockNotification")
}
