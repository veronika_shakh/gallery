//
//  PhotoManager.swift
//  Gallery
//
//  Created by Veranika Shakh on 20/01/2022.
//

import Foundation

class PhotoManager {
    
    static let shared = PhotoManager.init()
    private init() {}
    
    private let keyPhoto = "photoInfo"
   
    func addPhoto(newPhoto: PhotoInfo) {
        var allPhotos = getPhotoInfo()
        allPhotos.append(newPhoto)
        UserDefaults.standard.set(encodable: allPhotos, forKey: keyPhoto)
    }
    
    func getPhotoInfo() -> [PhotoInfo]  {         
        let allPhotos = UserDefaults.standard.value([PhotoInfo].self, forKey: keyPhoto)
        return allPhotos ?? []
    }
    
    func deletePhoto(photo: PhotoInfo) {
        var photos = getPhotoInfo()
        photos.removeAll { photoInfo in
            photoInfo.imageName == photo.imageName
        }
        UserDefaults.standard.set(encodable: photos, forKey: keyPhoto)
    }
    
    func updatePhoto(photo: PhotoInfo) {
        var allPhotos = getPhotoInfo()
        let index = allPhotos.firstIndex { photoInfo in
            photoInfo.imageName == photo.imageName
        }
        if let index = index {
            allPhotos[index] = photo
            UserDefaults.standard.set(encodable: allPhotos, forKey: keyPhoto)
        }
    }
}
