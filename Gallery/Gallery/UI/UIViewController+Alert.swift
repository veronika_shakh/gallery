//
//  UIAlertController+Utils.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import UIKit

extension UIViewController {
    
    func showAlertError(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { _ in }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }

    func showAlertDelete(title: String? = nil,
                         message: String? = nil,
                         destructiveButtonTitle: String? = nil,
                         destructiveHandler: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
        })
            alert.addAction(cancelAction)
        
        if destructiveButtonTitle != nil {
            let destructive = UIAlertAction(title: destructiveButtonTitle, style: .destructive)  { _ in
                destructiveHandler?()
            }
            alert.addAction(destructive)
        }
        present(alert, animated: true, completion: nil)
    }
}

