//
//  StartViewController.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import UIKit


class StartViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    // MARK: - Properties
    
    private var buttons: [UIButton] {
        [loginButton, registrationButton]
    }
    
    // MARK: - Lifecycle
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        buttons.forEach { button in
            button.roundCorners(radius: button.frame.height / 2, borderWidth: 2, borderColor: .white)
        }
    }
    
    // MARK: - Action
    
    @IBAction func loginButtonDidTap(_ sender: UIButton) {
        if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    @IBAction func registrationButtonDidTap(_ sender: UIButton) {
        if let registrationVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController {
            navigationController?.pushViewController(registrationVC, animated: true)
        }
    }
}
