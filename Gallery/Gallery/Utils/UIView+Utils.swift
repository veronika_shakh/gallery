//
//  UIView+Utils.swift
//  Gallery
//
//  Created by Veranika Shakh on 18/01/2022.
//


import UIKit

extension UIView {
    
    func roundCorners(radius: CGFloat = 20, borderWidth: CGFloat = 2, borderColor: UIColor? = nil) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = borderWidth
    }
}
