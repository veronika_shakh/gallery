//
//  HomeCollectionViewCell.swift
//  Gallery
//
//  Created by Veranika Shakh on 11/02/2022.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    func configurate(with photo: PhotoInfo) {
        photoImageView.image = FileManager.default.loadImage(fileName: photo.imageName)
    }
}
