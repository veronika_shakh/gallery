//
//  RegistrationViewController.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import UIKit

class RegistrationViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var logintiextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Lifecycle
  
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        saveButton.roundCorners(radius: saveButton.frame.height / 2, borderWidth: 2, borderColor: .white)
    }
    
    // MARK: - Action
    
    @IBAction func saveButtonDidTap(_ sender: UIButton) {
        guard let userName = self.nameTextField.text,
              let login = self.logintiextField.text,
              let password = self.passwordTextField.text
        else { return }
        
        AuthManager.shared.registerUser(userName: userName, login: login, password: password)
        
        if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController"),
           let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = homeVC
        }
    }
}
