//
//  Registration.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import Foundation

class UserInfo: Codable {
    
    var userName: String
    var login: String
 
    init(userName: String, login: String) {
        self.userName = userName
        self.login = login
    }
        
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(userName, forKey: .userName)
        try container.encode(login, forKey: .login)
    }
        
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.userName = try container.decode(String.self, forKey: .userName)
        self.login = try container.decode(String.self, forKey: .login)
    }
    
    private enum CodingKeys: String, CodingKey {
        case userName
        case login
    }
}
