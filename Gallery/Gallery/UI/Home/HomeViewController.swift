//
//  HomeViewController.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    // MARK: - Properties
    
    private var photos: [PhotoInfo] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Hello, \(AuthManager.shared.getUserName() ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        photos = PhotoManager.shared.getPhotoInfo()
        photoCollectionView.reloadData()
    }
}

// MARK: - UIImagePickerControllerDelegate

extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var imageToSave: UIImage?
        
        if let image = info[.editedImage] as? UIImage {
            imageToSave = image
        } else if let image = info[.originalImage] as? UIImage {
            imageToSave = image
        }
        
        if let imageToSave = imageToSave,
           let imageName = FileManager.default.saveImage(imageToSave) {
            let photoInfo = PhotoInfo(imageName: imageName, comment: "", isFavorite: false)
            PhotoManager.shared.addPhoto(newPhoto: photoInfo)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDataSource

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            guard
                let addCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPhotoCollectionViewCell", for: indexPath) as? AddPhotoCollectionViewCell
            else {
                return UICollectionViewCell()
            }
            return addCell
        } else {
            guard
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell
            else {
                return UICollectionViewCell()
            }
            cell.configurate(with: photos[indexPath.item - 1])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
        } else {
            if let photoVC = self.storyboard?.instantiateViewController(withIdentifier: "PhotosViewController") as? PhotosViewController {
                let index = indexPath.item - 1
                photoVC.index = index
                self.navigationController?.pushViewController(photoVC, animated: true)
            }
            collectionView.deselectItem(at: indexPath, animated: true)
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (photoCollectionView.frame.size.width - 20) / 3
        return CGSize(width: side, height: side)
    }
}
