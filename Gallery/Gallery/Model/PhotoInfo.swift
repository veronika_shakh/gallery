//
//  PhotoInfo.swift
//  Gallery
//
//  Created by Veranika Shakh on 20/01/2022.
//

import Foundation

class PhotoInfo: Codable {
    
    var imageName: String
    var comment: String
    var isFavorite: Bool
 
    init(imageName: String, comment: String, isFavorite: Bool) {
        self.imageName = imageName
        self.comment = comment
        self.isFavorite = isFavorite
    }
        
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(imageName, forKey: .imageName)
        try container.encode(comment, forKey: .comment)
        try container.encode(isFavorite, forKey: .isFavorite)
    }
        
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.imageName = try container.decode(String.self, forKey: .imageName)
        self.comment = try container.decode(String.self, forKey: .comment)
        self.isFavorite = try container.decode(Bool.self, forKey: .isFavorite)
    }
    
    private enum CodingKeys: String, CodingKey {
        case imageName
        case comment
        case isFavorite
    }
}
