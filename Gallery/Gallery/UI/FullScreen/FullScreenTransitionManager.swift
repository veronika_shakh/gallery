//
//  FullScreenTransitionManager.swift
//  Gallery
//
//  Created by Veranika Shakh on 02/02/2022.
//

import Foundation
import UIKit

class FullScreenTransitionManager: NSObject {
    
    // MARK: - Properties
    
    private var interactiveTransition = UIPercentDrivenInteractiveTransition()
    
    private let animationDuration = 0.3
    
    // MARK: - Private func
    
    private func showAnimation(fromVC: PhotosViewController,
                               toVC: FullScreenPhotoViewController,
                               transitionContext: UIViewControllerContextTransitioning) {
        
        let tempImageView = UIImageView()
        tempImageView.contentMode = .scaleAspectFit
        tempImageView.image = fromVC.photoImageView.image
        
        toVC.interactiveTransition = interactiveTransition
        toVC.view.addSubview(tempImageView)
        toVC.view.layoutSubviews()
        tempImageView.frame = fromVC.tempImageViewFrame

        UIView.animate(withDuration: animationDuration) {
            tempImageView.frame = toVC.photoImageViewInitialFrame
        } completion: { _ in
            transitionContext.completeTransition(true)
            toVC.photoImageView.alpha = 1
            tempImageView.removeFromSuperview()
        }        
    }
    
    private func dismissAnimation(fromVC: FullScreenPhotoViewController,
                                  transitionContext: UIViewControllerContextTransitioning,
                                  toVC: PhotosViewController) {
        
        let tempImageView = UIImageView()
        tempImageView.contentMode = .scaleAspectFit
        tempImageView.image = fromVC.photoImageView.image
        fromVC.view.addSubview(tempImageView)
        tempImageView.frame = fromVC.photoImageView.frame
        
        toVC.photoImageView.alpha = 0
        fromVC.photoImageView.alpha = 0
        
        UIView.animate(withDuration: animationDuration) {
            tempImageView.frame = toVC.tempImageViewFrame
            fromVC.view.backgroundColor = .clear
            fromVC.scrollView.alpha = 0

        } completion: { _ in
            transitionContext.completeTransition(true)
            fromVC.view.alpha = 0
            toVC.photoImageView.alpha = 1
            tempImageView.removeFromSuperview()
        }  
    }
}

//MARK: - UIViewControllerTransitioningDelegate

extension FullScreenTransitionManager: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

//MARK: - UIViewControllerAnimatedTransitioning

extension FullScreenTransitionManager: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        
        if let toVC = transitionContext.viewController(forKey: .to) as? FullScreenPhotoViewController,
           let fromNavC = transitionContext.viewController(forKey: .from) as? UINavigationController,
           let fromVC = fromNavC.topViewController as? PhotosViewController {
            containerView.addSubview(toVC.view)
            showAnimation(fromVC: fromVC, toVC: toVC, transitionContext: transitionContext)
        }
        
        else if let fromVC = transitionContext.viewController(forKey: .from) as? FullScreenPhotoViewController,
                let fromNavC = transitionContext.viewController(forKey: .to) as? UINavigationController,
                let toVC = fromNavC.topViewController as? PhotosViewController {
            containerView.addSubview(fromVC.view)
            dismissAnimation(fromVC: fromVC, transitionContext: transitionContext, toVC: toVC)
        }
    } 
}
