//
//  LoginViewController.swift
//  Gallery
//
//  Created by Veranika Shakh on 17/01/2022.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var okButton: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginTextField.text = AuthManager.shared.getUserLogin()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveAccountLockNotification),
                                               name: .accountLocked,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveAccountUnlockNotification),
                                               name: .accountUnLocked,
                                               object: nil)
        if AuthManager.shared.attemptsCount == AuthManager.shared.maxAttemptsCount {
            didReceiveAccountLockNotification()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        okButton.roundCorners(radius: okButton.frame.height / 2, borderWidth: 2, borderColor: .white)
    }
   
    // MARK: - Action
    
    @IBAction func okButtonDidTap(_ sender: UIButton) {
        guard let login = self.loginTextField.text,
              let password = self.passwordTextField.text
        else { return }
        
        if AuthManager.shared.login(login: login, password: password) {
            if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController"),
               let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.window?.rootViewController = homeVC
            }
        } else {
            var message = ""
            let attemptsLeft = AuthManager.shared.maxAttemptsCount - AuthManager.shared.attemptsCount
            
            if attemptsLeft > 1 {
                message = "Password are incorrect.You have \(attemptsLeft) attempts left. Please, try again."
            } else if attemptsLeft == 0 {
                message = "Login limit exceeded. You can try in \(AuthManager.shared.timeInterval) seconds."
            } else {
                message = "Password are incorrect.You have \(attemptsLeft) attempt left. Please, try again."
            }
            showAlertError(title: "ERROR", message: message)
        }
    }
    
    @objc func didReceiveAccountLockNotification() {
        okButton.isEnabled = false
        passwordTextField.text = ""
        passwordTextField.isEnabled = false
    }
    
    @objc func didReceiveAccountUnlockNotification() {
        okButton.isEnabled = true
        passwordTextField.isEnabled = true
    } 
}
