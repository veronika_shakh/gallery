//
//  PhotosViewController.swift
//  Gallery
//
//  Created by Veranika Shakh on 18/01/2022.
//

import UIKit

class PhotosViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var index: Int = 0
    var tempImageViewFrame: CGRect!
    
    private var photos: [PhotoInfo] = []
    
    private var transitionManager = FullScreenTransitionManager()
    private var floatingContainerView: UIView!
    private var floatingImageView: UIImageView!
    
    private var buttons: [UIButton] {
        [addButton, leftButton, rightButton, likeButton, deleteButton]
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        floatingContainerView = UIView(frame: CGRect(x: view.frame.width,
                                                     y: 0,
                                                     width: photoImageView.frame.width,
                                                     height: photoImageView.frame.height))
        floatingContainerView.backgroundColor = UIColor(named: "beigeColor")
        
        floatingImageView = UIImageView(frame: CGRect(x: 0,
                                                      y: 0,
                                                      width: photoImageView.frame.width,
                                                      height: photoImageView.frame.height))
        floatingImageView.contentMode = .scaleAspectFit
        floatingContainerView.addSubview(floatingImageView)
        contentView.addSubview(floatingContainerView)
        
        registerForKeyboardNotifications()
        reloadPhotos()
        addRecognizer()
        imageDidChange()
        
        photoImageView.image = imageForIndex(index)
        photoImageView.contentMode = .scaleAspectFit
        
        textField.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateButtonsLayout()
    }
    
    // MARK: - Action
    
    @IBAction func likeButtonDidTap(_ sender: UIButton) {
        let photoInfo = photos[index]
        photoInfo.isFavorite = !photoInfo.isFavorite
        PhotoManager.shared.updatePhoto(photo: photoInfo)
    
        reloadPhotos()
        updateLikeButtonImage()
    }
    
    @IBAction func addButtonDidTap(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func deleteButtonDidTap(_ sender: UIButton) {
        showAlertDelete (title: "Delete image?",
                         message: "This image will be permanently deleted from this gallery.",
                         destructiveButtonTitle: "Delete",
                         destructiveHandler: {
            PhotoManager.shared.deletePhoto(photo: self.photos[self.index])
            self.reloadPhotos()
            self.showPreviousImage()
        })
    }
    
    @IBAction func rightArrowButtonDidTap(_ sender: UIButton) {
        showNextImage()
    }
    
    @IBAction func leftArrowButtonDidTap(_ sender: UIButton) {
        showPreviousImage()
    }
    
    // MARK: - Private func
    
    private func updateButtonsLayout() {
        buttons.forEach { button in
            button.roundCorners(radius: button.frame.height / 2,
                                borderWidth: 2,
                                borderColor: .black)
        }
    }
    
    // MARK: - Func from ImageView
    
    private func showNextImage() {
        guard photos.count > 0 else { return }
        index += 1
        if index >= photos.count {
            index = 0
        }
        
        floatingContainerView.frame = CGRect(x: view.frame.width,
                                             y: imageContainerView.frame.origin.y,
                                             width: imageContainerView.frame.width,
                                             height: imageContainerView.frame.height)
        floatingImageView.frame = photoImageView.frame
        
        let imageToShow = imageForIndex(index)
        floatingImageView.image = imageToShow
        UIView.animate(withDuration: 0.3) {
            self.floatingContainerView.frame.origin = self.imageContainerView.frame.origin
        } completion: { _ in
            self.imageDidChange()
            self.photoImageView.image = imageToShow
        }
    }
    
    private func showPreviousImage() {
        floatingContainerView.frame.origin = self.imageContainerView.frame.origin
        floatingImageView.frame = photoImageView.frame
        floatingImageView.image = photoImageView.image
        decrementIndex()
        photoImageView.image = imageForIndex(index)
        UIView.animate(withDuration: 0.3) {
            self.floatingContainerView.frame.origin.x = -self.view.frame.width
        } completion: { _ in
            self.imageDidChange()
        }
    }
    
    private func showLastImage() {
        let lastImageIndex = photos.count - 1
        self.index = lastImageIndex

        photoImageView.image = imageForIndex(index)
        floatingContainerView.frame = CGRect(x: view.frame.width,
                                             y: 0,
                                             width: imageContainerView.frame.width,
                                             height: imageContainerView.frame.height)
        imageDidChange()
    }
    
    private func imageDidChange() {
        updateLikeButtonImage()
        updateComment()
        textLabel.isHidden = photos.count != 0
    }
    
    private func decrementIndex() {
        if index <= 0 {
            index = photos.count
        }
        index -= 1
    }
    
    private func imageForIndex(_ index: Int) -> UIImage? {
        guard photos.count > index, index >= 0  else {
            return nil
        }
        let photoInfo = photos[index]
        let image = FileManager.default.loadImage(fileName: photoInfo.imageName)
        return image
    }
    
    // MARK: - Func to read saved
    
    private func reloadPhotos() {
        photos = PhotoManager.shared.getPhotoInfo()
        let elements = [textField, likeButton, deleteButton, rightButton, leftButton]
        if photos.count == 0 {
            elements.forEach { element in
                element?.alpha = 0.3
                element?.isEnabled = false
            }
        } else {
            elements.forEach { element in
                element?.alpha = 1
                element?.isEnabled = true
            }
        }
    }
    
    private func updateLikeButtonImage() {
        guard photos.count > index, index >= 0 else {
            likeButton.setImage(UIImage.defaultHeart, for: .normal)
            return
        }
        let photoInfo = photos[index]
        if photoInfo.isFavorite == false {
            likeButton.setImage(UIImage.defaultHeart, for: .normal)
        } else {
            likeButton.setImage(UIImage.favoriteHeart, for: .normal)
        }
    }
    
    private func updateComment() {
        guard photos.count > index, index >= 0  else {
            textField.text = nil
            return
        }
        let photoInfo = self.photos[index]
        textField.text = photoInfo.comment
    }
    
    //MARK: - Func for keyboard
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @IBAction func keyboardWillShow(_ notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else { return }
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomConstraint.constant = 0
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification {
            bottomConstraint.constant = keyboardScreenEndFrame.height + 15 - 120
        }
        
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func tapDetected(recognizer: UITapGestureRecognizer) {
        if let fullScreenPhotosVC = self.storyboard?.instantiateViewController(withIdentifier: "FullScreenPhotosViewController") as? FullScreenPhotoViewController {
            let photoInfo = photos[index]
            fullScreenPhotosVC.photoInfo = photoInfo
            tempImageViewFrame = view.convert(photoImageView.frame, from: imageContainerView)
            fullScreenPhotosVC.transitioningDelegate = transitionManager
            fullScreenPhotosVC.modalPresentationStyle = .custom
            present(fullScreenPhotosVC, animated: true)
        }
    }
        
    func addRecognizer() {
        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(tapDetected(recognizer:)))
        photoImageView.addGestureRecognizer(recognizer)
    }
}

//MARK: - UITextFieldDelegate

extension PhotosViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        
        let photoInfo = photos[index]
        if let text = textField.text {
            photoInfo.comment = text
            PhotoManager.shared.updatePhoto(photo: photoInfo)
        }
        return true
    }
}

//MARK: - UIImagePickerControllerDelegate

extension PhotosViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var imageToSave: UIImage?
        if let image = info[.editedImage] as? UIImage {
            imageToSave = image
        } else if let image = info[.originalImage] as? UIImage {
            imageToSave = image
        }
        if let imageToSave = imageToSave,
           let imageName = FileManager.default.saveImage(imageToSave) {
            let photoInfo = PhotoInfo(imageName: imageName, comment: "", isFavorite: false)
            PhotoManager.shared.addPhoto(newPhoto: photoInfo)
            reloadPhotos()
            showLastImage()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UIImage + Favorite

extension UIImage {
    static let defaultHeart = UIImage(named: "like")
    static let favoriteHeart = UIImage(named: "redLike")
}
