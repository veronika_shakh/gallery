//
//  FullScreenPhotoViewController.swift
//  Gallery
//
//  Created by Veranika Shakh on 02/02/2022.
//

import UIKit

class FullScreenPhotoViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: ImageZoomView!

    // MARK: - Properties
    
    var photoInfo: PhotoInfo!
    var interactiveTransition: UIPercentDrivenInteractiveTransition!
    
    var photoImageView: UIImageView {
        return scrollView.imageView
    }
    
    var photoImageViewInitialFrame: CGRect {
        return scrollView.convert(photoImageView.frame, to: scrollView)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let zoomImage = FileManager.default.loadImage(fileName: photoInfo.imageName)
        scrollView.image = zoomImage
        scrollView.imageView.alpha = 0
        
        addRecognizer()
    }
    
    // MARK: - Action
    
    @IBAction func tapDetected(recognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func addRecognizer() {
        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(tapDetected(recognizer:)))
        recognizer.delegate = self
        scrollView.addGestureRecognizer(recognizer)
    }
}

    //MARK: - UIGestureRecognizerDelegate

extension FullScreenPhotoViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
